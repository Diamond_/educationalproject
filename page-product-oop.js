"use script"

class Form {
    constructor(input) {
        this.input = input;
    }

    handlerSubmit(e) {
        e.preventDefault();
        localStorage.removeItem(input.getAttribute('name'));
        alert('Форма успешно отправлена!');
    }

    handlerInput(e) {
        let value = e.target.value;
        let name = e.target.getAttribute('name');
        localStorage.setItem(name, value);
    }
}

class AddReviewForm extends Form {

    form = document.querySelector(".add-review__form");
    fieldName = document.querySelector(".form__name");
    fieldGrade = document.querySelector(".form__grade");
    nameErrorElem = document.querySelector(".name__error");
    gradeErrorElem = document.querySelector(".grade__error");
    textArea = document.querySelector(".form__text-area");
    fieldTextArea = document.querySelector('.text-area__field');
    input = form.querySelectorAll('.input');

    clearNameError() {
        nameErrorElem.innerText = "";
        fieldName.style.border = "1px solid #888888";
        nameErrorElem.style.padding = "0";
    }

    clearGradeError() {
        gradeErrorElem.innerText = ""
        fieldGrade.style.border = "1px solid #888888";
        gradeErrorElem.style.padding = "0";
    }

    handlerSubmit(e) {
        e.preventDefault();

        if (fieldName.value.length === 0) {
            clearGradeError();
            nameErrorElem.innerText = "Вы забыли указать имя и фамилию";
            fieldName.style.border = "1px solid #DA4A0C";
            nameErrorElem.style.padding = "8px 18px";
        } else if (fieldName.value.length < 3) {
            clearGradeError();
            nameErrorElem.innerText = "Имя не может быть короче 2-хсимволов";
            fieldName.style.border = "1px solid #DA4A0C";
            nameErrorElem.style.padding = "8px 18px";
        } else if (fieldGrade.value === "" ||
            fieldGrade.value < 1 ||
            fieldGrade.value > 5 ||
            +fieldGrade.value != fieldGrade.value) {
            clearNameError();
            gradeErrorElem.innerText = "Оценка должна быть от 1 до 5";
            fieldGrade.style.border = "1px solid #DA4A0C";
            gradeErrorElem.style.padding = "8px 18px";
        } else {
            clearNameError();
            clearGradeError();
        }

        for (let i = 0; i < input.length; i++) {
            localStorage.removeItem(input[i].getAttribute('name'));
        }
    }

    handlerInput(e) {
        for (let i = 0; i < input.length; i++) {
            let value = e.target.value;
            let name = e.target.getAttribute('name');
            localStorage.setItem(name, value);
        }
    }

    main() {
        for (let i = 0; i < input.length; i++) {
            let name = input[i].getAttribute('name');
            input[i].value = localStorage.getItem(name);
        }

        form.addEventListener("submit", handlerSubmit);
        for (let i = 0; i < input.length; i++) {
            input[i].addEventListener("input", handlerInput);
        }
    }
}