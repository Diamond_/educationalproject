"use script"

// Упражнение 1
for (let i = 0; i <= 20; i += 2) {
    console.log(i);
}

// Упражнение 2
let calcNumbers = Number(0);
for (let i = 0; i < 3; i++) {
    let number = prompt(`Введите ${i + 1}-е число:`);
    if (number == +number) {
        calcNumbers = calcNumbers + +number;
    } else {
        alert("Ошибка, вы ввели не число");
        break;
    }
}
console.log(calcNumbers);

// Упражнение 3
function getNameOfMonth(month) {
    switch (month) {
        case 0:
            console.log("Январь");
            break;
        case 1:
            console.log("Февраль");
            break;
        case 2:
            console.log("Март");
            break;
        case 3:
            console.log("Апрель");
            break;
        case 4:
            console.log("Май");
            break;
        case 5:
            console.log("Июнь");
            break;
        case 6:
            console.log("Июль");
            break;
        case 7:
            console.log("Август");
            break;
        case 8:
            console.log("Сентябрь");
            break;
        case 9:
            console.log("Октябрь");
            break;
        case 10:
            console.log("Ноябрь");
            break;
        case 11:
            console.log("Декабрь");
            break;
    }
}
for (let i = 0; i < 12; i++) {
    if (i === 9) {
        continue;
    }
    getNameOfMonth(i);
}

// Упражнение 4
(function () {
    alert("IIFE - это паттерн, который функцию превращает в выражение и моментально вызывает её!");
}());