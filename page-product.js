"use script"

let form = document.querySelector(".add-review__form");
let fieldName = document.querySelector(".form__name");
let fieldGrade = document.querySelector(".form__grade");
let nameErrorElem = document.querySelector(".name__error");
let gradeErrorElem = document.querySelector(".grade__error");
let textArea = document.querySelector(".form__text-area");
let fieldTextArea = document.querySelector('.text-area__field');
let input = form.querySelectorAll('.input');

function clearNameError() {
    nameErrorElem.innerText = "";
    fieldName.style.border = "1px solid #888888";
    nameErrorElem.style.padding = "0";
}

function clearGradeError() {
    gradeErrorElem.innerText = ""
    fieldGrade.style.border = "1px solid #888888";
    gradeErrorElem.style.padding = "0";
}

function handlerSubmit(e) {
    e.preventDefault();

    if (fieldName.value.length === 0) {
        clearGradeError();
        nameErrorElem.innerText = "Вы забыли указать имя и фамилию";
        fieldName.style.border = "1px solid #DA4A0C";
        nameErrorElem.style.padding = "8px 18px";
    } else if (fieldName.value.length < 3) {
        clearGradeError();
        nameErrorElem.innerText = "Имя не может быть короче 2-хсимволов";
        fieldName.style.border = "1px solid #DA4A0C";
        nameErrorElem.style.padding = "8px 18px";
    } else if (fieldGrade.value === "" ||
        fieldGrade.value < 1 ||
        fieldGrade.value > 5 ||
        +fieldGrade.value != fieldGrade.value) {
        clearNameError();
        gradeErrorElem.innerText = "Оценка должна быть от 1 до 5";
        fieldGrade.style.border = "1px solid #DA4A0C";
        gradeErrorElem.style.padding = "8px 18px";
    } else {
        clearNameError();
        clearGradeError();
    }

    for (let i = 0; i < input.length; i++) {
        localStorage.removeItem(input[i].getAttribute('name'));
    }
}

function handlerInput(e) {
    for (let i = 0; i < input.length; i++) {
        let value = e.target.value;
        let name = e.target.getAttribute('name');
        localStorage.setItem(name, value);
    }
}

for (let i = 0; i < input.length; i++) {
    let name = input[i].getAttribute('name');
    input[i].value = localStorage.getItem(name);
}

form.addEventListener("submit", handlerSubmit);
for (let i = 0; i < input.length; i++) {
    input[i].addEventListener("input", handlerInput);
}