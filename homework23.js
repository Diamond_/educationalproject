"use script"

// Упражнение 1
let number = prompt("Введите число");
if (+number != number) {
    alert("ОШИБКА!!! Вы ввели не число.")
} else {
    let promise = new Promise(function (resolve) {
        let specialId = setInterval(() => {
            if (number <= 0) {
                console.log("Время вышло!");
                clearInterval(specialId);
            } else {
                console.log(number);
                number--;
            }
        }, 1000);
    });
}

// Упражнение 2
let promise = fetch("https://reqres.in/api/users");

promise
    .then(function (response) {
        return response.json;
    })

    .then(function (response) {
        console.log("Получили пользователей: " + response.per_page);
        for (let i = 0; i < response.data.length; i++) {
            console.log("-" + response.data[i].first_name + response.data[i].last_name + "(" + response.data[i].email + ")");
        }
    })

    .catch(function () {
        console.log("Кажется бэкенд сломался :(");
    });