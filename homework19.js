"use script";

// Упражнение 1
let a = '$100'
let b = '300$'

let sum = +a.slice(1, a.length) + parseInt(b);
console.log(sum);

// Упражнение 2
let message = ' привет, медвед      ';
console.log(message.trim().replace('привет', 'Привет'));

// Упражнение 3
let age = prompt("Сколько вам лет?");
if (age >= 0 && age <= 3) {
    alert(`Вам ${age} лет и вы младенец!`);
} else if (age > 3 && age <= 11) {
    alert(`Вам ${age} лет и вы ребенок!`);
} else if (age > 11 && age <= 18) {
    alert(`Вам ${age} лет и вы подросток!`);
} else if (age > 18 && age <= 40) {
    alert(`Вам ${age} лет и вы познаёте жизнь!`);
} else if (age > 40 && age <= 80) {
    alert(`Вам ${age} лет и вы познали жизнь!`);
} else if (age > 80) {
    alert(`Вам ${age} лет и вы долгожитель!`);
}

// Упражнение 3
let msg = 'Я работаю со строками как профессионал!';
console.log(msg.split(" ").length);