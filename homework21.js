"use script"

// Упражнение 1
function isEmpty(obj) {
    let i = 0;
    for (let key in obj) {
        i++;
    }
    if (i == 0) return false;
    else return true;
}
let user = {
    name: "asdef",
    asd: 'asd',
    asdwee: 1245,
};
console.log(isEmpty(user));

// Упражнение 2
let product = {
    name: "Смартфон Apple iPhone 13",
    color: "Синий",
    memory: "256Gb",
    display: 6.1,
    OS: "iOS 15",
    wirelessInterfaces: "NFC, Bluetooth, Wi-Fi",
    CPU: "Apple A15 Bionic",
    weight: "173 г",
    description: `Наша самая совершенная система двух камер.
    Особый взгляд на прочность дисплея.
    Чип, с которым всё супербыстро.
    Аккумулятор держится заметно дольше.
    iPhone 13 - сильный мира всего.`,
    price: 67990,
    oldPrice: 75990,
    discount: "8%",
};

let firstReview = {
    name: "Марк Г.",
    fotoLink: "./images/autor-1.svg",
    reviewRating: 5,
    reviewText: `Достоинства:
    это мой первый айфон после после огромного количества телефонов на андроиде. всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая,ширик тоже навысоте.
    
    Недостатки:
    к самому устройству мало имеет отношение, но перенос данных с андроида - адскаявещь) а если нужно переносить фото с компа, то это только через itunes, которыйурезает качество фотографий исходное`
};
let secondReview = {
    name: "Валерий Коваленко",
    fotoLink: "./images/autor-2.svg",
    reviewRating: 4,
    reviewText: `Достоинства:
    OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго
    
    Недостатки:
    Плохая ремонтопригодность`
};

// Упражнение 3
let salaries = {
    John: 100000,
    Ann: 160000,
    Pete: 130000,
};

function raiseSalary(perzent, object) {
    for (let key in object) {
        object[key] = object[key] * perzent / 100 + object[key];
        console.log(object[key]);
    }
}
raiseSalary(5, salaries);