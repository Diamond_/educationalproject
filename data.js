"use script"

let product = {
    name: "Смартфон Apple iPhone 13",
    color: "Синий",
    memory: "256Gb",
    display: 6.1,
    OS: "iOS 15",
    wirelessInterfaces: "NFC, Bluetooth, Wi-Fi",
    CPU: "Apple A15 Bionic",
    weight: "173 г",
    description: `Наша самая совершенная система двух камер.
    Особый взгляд на прочность дисплея.
    Чип, с которым всё супербыстро.
    Аккумулятор держится заметно дольше.
    iPhone 13 - сильный мира всего.`,
    price: 67990,
    oldPrice: 75990,
    discount: "8%",
    imegesArray: ["./images/image-1.webp", "./images/image-2.webp", "./images/image-3.webp", "./images/image-4.webp", "./images/image-5.webp"],
    colorArray: ["red", "green", "pink", "blue", "white", "black"],
    memoryArray: ["128Gb", "256Gb", "512Gb"],
    deliveryArray: [{ name: "Самовывоз", date: "четверг, 1 сентября", cost: 0 },
    { name: "Курьером", date: "четверг, 1 сентября", cost: 0 }],
};

let firstReview = {
    name: "Марк Г.",
    fotoLink: "./images/autor-1.svg",
    reviewRating: 5,
    reviewText: `Достоинства:
    это мой первый айфон после после огромного количества телефонов на андроиде. всё плавно, чётко и красиво. довольно шустрое устройство. камера весьма неплохая,ширик тоже навысоте.
    
    Недостатки:
    к самому устройству мало имеет отношение, но перенос данных с андроида - адскаявещь) а если нужно переносить фото с компа, то это только через itunes, которыйурезает качество фотографий исходное`
};
let secondReview = {
    name: "Валерий Коваленко",
    fotoLink: "./images/autor-2.svg",
    reviewRating: 4,
    reviewText: `Достоинства:
    OLED экран, Дизайн, Система камер, Шустрый А15, Заряд держит долго
    
    Недостатки:
    Плохая ремонтопригодность`
};