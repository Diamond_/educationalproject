"use script"

// Упражнение 1
let a = '100px';
let b = '323px';

let result = parseInt(a) + parseInt(b);
console.log(result);

// Упражнение 2
console.log(Math.max(10, -45, 102, 36, 12, 0, -1));

// Упражнение 3
let c = 123.3399;// Округлить до 123
// Решение
console.log("c = " + Math.round(c));

let d = 0.111;// Округлить до 1
// Решение
console.log("d = " + Math.ceil(d));

let e = 45.333333;// Округлить до 45.3
// Решение
console.log("e = " + e.toFixed(1));

let f = 3;// Возвести в степень 5 (должно быть 243)
// Решение
console.log("f = " + f ** 5);

let g = 4e14;// Записать в сокращенномвиде
// Решение
console.log("g = " + g);

let h = '1' == 1;// Поправить условие, чтобы результатбыл true (значения изменять нельзя, только оператор)
// Решение
console.log("h = " + h);

// Упражнение 4
console.log(0.1 + 0.2 === 0.3); // Потеря точности в двоичной системе