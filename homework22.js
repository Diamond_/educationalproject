"use script"

// Упражнение 1
function getSumm(arr) {
    let sumArr = 0;
    for (let i = 0; i < arr.length; i++) {
        if (typeof arr[i] === 'number') {
            sumArr = arr[i] + sumArr;
        } else continue;
    }
    console.log(sumArr);
}
let firstArr = [1, 2, 3, 'a', 1, {}];
getSumm(firstArr);

// Упражнение 3
let basket = new Set();

function addToCart(index) {
    basket.add(index);
}

function removeFromCart(index) {
    basket.delete(index);
}

addToCart(1);
addToCart(12);
addToCart(13);
addToCart(1);

basket.forEach((product) => console.log(product));

removeFromCart(1);

basket.forEach((product) => console.log(product));